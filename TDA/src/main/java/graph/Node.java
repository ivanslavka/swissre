package graph;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by S9KH4M on 19. 10. 2017.
 */
public final class Node<T, R> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Unique id of the node
     */
    private T value;
    /**
     * Execution result of this node
     */
    private R result;
    /**
     * Execution status of this node
     */
    private boolean status = false;
    /**
     * Arbitray data of this node
     */
    private Object data;
    /**
     * incoming dependencies for this node
     */
    private Set<Node<T, R>> inComingEdges = new LinkedHashSet<Node<T, R>>();
    /**
     * outgoing dependencies for this node
     */
    private Set<Node<T, R>> outGoingEdges = new LinkedHashSet<Node<T, R>>();
    /**
     * Constructs the node with the given node Id
     * @param val the new unique id
     */
    public Node(final T val) {
        this.value = val;
    }
    /**
     * Add the given node, to the set of incoming nodes
     * @param node add as dependency to the node
     */
    public void addInComingNode(final Node<T, R> node) {
        this.inComingEdges.add(node);
    }
    /**
     * add the given to the set of out going nodes
     * @param node add as dependency to the node
     */
    public void addOutGoingNode(final Node<T, R> node) {
        this.outGoingEdges.add(node);
    }
    /**
     *
     * @return the set of incoming nodes
     */
    public Set<Node<T, R>> getInComingNodes() {
        return this.inComingEdges;
    }
    /**
     *
     * @return set of out going nodes
     */
    public Set<Node<T, R>> getOutGoingNodes() {
        return this.outGoingEdges;
    }
    /**
     *
     * @return the node's value
     */
    public T getValue() {
        return this.value;
    }
    /**
     *
     * @return the node's execution result
     */
    public R getResult() {
        return result;
    }
    /**
     * @param result the new result
     * sets the node's execution result to a new value
     */
    public void setResult(final R result) {
        this.result = result;
    }
    /**
     *
     * @return {@code true} if the node is non processed
     */
    public boolean isNotProcessed() {
        return !isProcessed();
    }

    public boolean isProcessed() {
        return this.status;
    }

    public void setProcessed(){this.status = true;}


    /**
     *
     * @return the node's data
     */
    public Object getData() {
        return data;
    }

    /**
     * @param data the data
     * Sets the node's data to a new value
     */
    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.value == null) ? 0 : this.value.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        @SuppressWarnings("unchecked")
        Node<T, R> other = (Node<T, R>) obj;

        return this.value.equals(other.value);
    }

    @Override
    public String toString() {
        return String.valueOf(this.value);
    }
    /**
     * Represents node's execution status
     * <ul>
     * 		<li> <code> ERRORED:</code> Node's execution was in error</li>
     * 		<li> <code> SKIPPED:</code>Node's  execution was skipped</li>
     * 		<li> <code> SUCCESS:</code>Node's  execution was success</li>
     * </ul>
     *
     * @author Nadeem Mohammad
     *
     */
}