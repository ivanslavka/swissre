package strategy;

import graph.Node;

/**
 * Created by S9KH4M on 19. 10. 2017.
 */
public class StatutoryProfitStrategy implements ICalculationStrategy {

    public static final String NAME = "STATUTORY_PROFIT";

    @Override
    public TaxCalculatorData getData() {
        System.out.println("Getting data for StatutoryProfitStrategy.");
        return null;
    }

    @Override
    public void execute(Node node) {
        System.out.println("Processing data for StatutoryProfitStrategy.");
        node.setResult("Data from StatutoryProfitStrategy");
    }
}
