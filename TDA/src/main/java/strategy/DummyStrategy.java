package strategy;


import graph.Node;

/**
 * Created by S9KH4M on 19. 10. 2017.
 */
public class DummyStrategy  implements ICalculationStrategy{

        private static final long serialVersionUID = 1L;

        public static final String NAME = "DUMMY";

    @Override
    public TaxCalculatorData getData() {
        return null;
    }

    @Override
    public void execute(Node node) {
        System.out.println("Processing data for DummyStrategy.");
        node.setResult("Data from DummyStrategy");
    }
}
