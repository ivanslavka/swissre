package strategy;

import graph.Node;
import model.RuleModel;
import org.jboss.weld.environment.se.Weld;
import org.jboss.weld.environment.se.WeldContainer;
import org.kie.api.KieServices;
import org.kie.api.cdi.KSession;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import javax.inject.Inject;

/**
 * Created by S9KH4M on 20. 10. 2017.
 */
public class RuleStrategy implements ICalculationStrategy {

    public static final String NAME = "RULE_STRATEGY";

    private KieSession kSession;

    @Override
    public TaxCalculatorData getData() {
        return null;
    }

    @Override
    public void execute(Node node) {
        System.out.println("Processing data for RuleStrategy.");
        //Boostrap the CDI container, in this case WELD
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kContainer = kieServices.getKieClasspathContainer();
        kSession = kContainer.newKieSession();

        RuleModel rm = new RuleModel();
        kSession.insert(rm);
        kSession.fireAllRules();

        node.setResult(rm.ruleOutput);
    }
}
