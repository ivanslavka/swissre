package strategy;

import graph.Node;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by S9KH4M on 19. 10. 2017.
 */
public class CapitalIncomeTaxStrategy implements ICalculationStrategy {

    public static final String NAME = "CAPITAL_INCOME";

    public TaxCalculatorData data;
    public List<ICalculationStrategy> predecessorList = new ArrayList<>();

    @Override
    public TaxCalculatorData getData() {
        System.out.println("Getting data for CapitalIncomeTaxStrategy.");
        return data;
    }

    @Override
    public void execute(Node node) {
        System.out.println("Processing data for CapitalIncomeTaxStrategy.");
        node.setResult("Data from CapitalIncomeTaxStrategy.");
    }
}
