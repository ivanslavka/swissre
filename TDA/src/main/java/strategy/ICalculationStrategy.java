package strategy;

import graph.Node;

/**
 * Created by S9KH4M on 19. 10. 2017.
 */
public interface ICalculationStrategy {

    public TaxCalculatorData getData();
    public void execute(Node node);
}
