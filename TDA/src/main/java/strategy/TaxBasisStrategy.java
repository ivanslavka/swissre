package strategy;

import graph.Node;

/**
 * Created by S9KH4M on 19. 10. 2017.
 */
public class TaxBasisStrategy implements ICalculationStrategy {

    public static final String NAME = "TAX_BASIS";

    @Override
    public TaxCalculatorData getData() {
        System.out.println("Getting data for TaxBasisStrategy.");
        return null;
    }

    @Override
    public void execute(Node node) {
        System.out.println("Processing data for TaxBasisStrategy.");
        node.setResult("Data from TaxBasisStrategy");
    }
}
