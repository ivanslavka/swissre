package app;

import graph.DefaultDag;
import graph.Node;
import org.kie.api.definition.rule.Rule;
import strategy.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by S9KH4M on 19. 10. 2017.
 */
public class App {

    public static void main(String[] args){
        Map<String, ICalculationStrategy> strategies = new HashMap<>();
        strategies.put(DummyStrategy.NAME, new DummyStrategy());
        strategies.put(TaxBasisStrategy.NAME, new TaxBasisStrategy());
        strategies.put(CapitalIncomeTaxStrategy.NAME, new CapitalIncomeTaxStrategy());
        strategies.put(StatutoryProfitStrategy.NAME, new StatutoryProfitStrategy());
        strategies.put(RuleStrategy.NAME, new RuleStrategy());

        DefaultDag<String, String> dag = new DefaultDag<>();
        dag.addDependency(DummyStrategy.NAME, TaxBasisStrategy.NAME);
        dag.addDependency(StatutoryProfitStrategy.NAME, TaxBasisStrategy.NAME);
        dag.addDependency(TaxBasisStrategy.NAME, CapitalIncomeTaxStrategy.NAME);
        dag.addDependency(TaxBasisStrategy.NAME, RuleStrategy.NAME);
        dag.addDependency(RuleStrategy.NAME, CapitalIncomeTaxStrategy.NAME);

        DagExecutor de = new DagExecutor(dag, strategies);
        de.execute();

//        Set<Node<String, String>> terminatingNodes = dag.getTerminatingNodes();
//        for(Node node : terminatingNodes){
//            displayNode(node);
//        }

        Node node = de.getProcessedNode(RuleStrategy.NAME);
        displayNode(node);

        displayNode(de.getProcessedNode(TaxBasisStrategy.NAME));
    }
    private static void displayNode(Node node){
        System.out.println("---------------------------------");
        System.out.println("Displaying node: " + node.getValue());
        System.out.println("Node result: " + node.getResult());
        String dependsOn = "Depends on:  ";

        for(Object incomingNode : node.getInComingNodes()){
            dependsOn += ((Node) incomingNode).getValue() + " ";
        }
        System.out.println(dependsOn);
        System.out.println("---------------------------------");
    }
}
