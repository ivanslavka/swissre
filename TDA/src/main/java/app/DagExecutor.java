package app;

import graph.Dag;
import graph.Node;
import strategy.ICalculationStrategy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by S9KH4M on 20. 10. 2017.
 */
public class DagExecutor {

    private Map<String, Node> processedNodes = new HashMap<>();
    private Map<String, ICalculationStrategy> strategies;
    private Dag dag;

    public DagExecutor(Dag dag, Map<String, ICalculationStrategy> strategies){
        this.strategies = strategies;
        this.dag = dag;
    }

    public void execute(){
        Set<Node<String, String>> initialNodes = dag.getInitialNodes();
        this.doExecute(initialNodes);
    }

    private void doExecute(Set<Node<String, String>> nodes){
        for(Node<String, String> node : nodes){

            if(node.isNotProcessed() && shouldExecute(node)) {
                ICalculationStrategy strategy = this.strategies.get(node.getValue());
                strategy.execute(node);
                node.setProcessed();
                processedNodes.put(node.getValue(), node);
            }
            doExecute(node.getOutGoingNodes());
        }
    }

    private boolean shouldExecute(Node<String, String> node){
        boolean shouldExecute = true;
        for(Node incomingNode : node.getInComingNodes()){
            if(incomingNode.isNotProcessed()){
                shouldExecute = false;
                break;
            }
        }
        return shouldExecute;
    }

    public Node getProcessedNode(String nodeName){
        return processedNodes.get(nodeName);
    }
}
