package bigshop;

import java.math.BigDecimal;

public class PhoneInsuranceItem extends AbstractItem{

	public static final String NAME = "phone insurance";

	private static final BigDecimal TAX_RATE = new BigDecimal(0);
	private static final BigDecimal ITEM_PRICE = new BigDecimal(120);

	public PhoneInsuranceItem() {
		super(NAME, ITEM_PRICE, TAX_RATE);
	}
}