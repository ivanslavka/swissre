package bigshop;

import java.math.BigDecimal;
import java.util.LinkedList;

public abstract class AbstractItem {

	final private String itemName;
	final private BigDecimal itemPrice;
	final private BigDecimal itemTaxRate;

	private static final BigDecimal PERCENTILE = new BigDecimal(0.01d);

	public AbstractItem(String itemName, BigDecimal itemPrice, BigDecimal itemTaxRate) {
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.itemTaxRate = itemTaxRate;
	}

	public void addItem(LinkedList<CartItem> itemList) {
		itemList.add(new CartItem(this, false));
	}

	public void addFreeItem(LinkedList<CartItem> itemList) {
		itemList.add(new CartItem(this, true));
	}

	public void removeItem(LinkedList<CartItem> itemList) {
		itemList.removeLast();
	}

	public BigDecimal getItemPrice() {
		return this.itemPrice;
	}

	public BigDecimal getItemTaxRate() {
		return this.itemTaxRate;
	}

	public String getItemName() {
		return this.itemName;
	}

	public BigDecimal calculateItemPrice() {
		return this.itemPrice.add(this.itemPrice.multiply(this.itemTaxRate).multiply(PERCENTILE)).setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}

	public BigDecimal calculateItemTax() {
		return this.itemPrice.multiply(this.itemTaxRate).multiply(PERCENTILE).setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}
}
