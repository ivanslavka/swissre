package bigshop;

import java.math.BigDecimal;

public class WirelessEarphonesItem extends AbstractItem {

	public static final String NAME = "wireless earphone";

	private static final BigDecimal ITEM_PRICE = new BigDecimal(50);
	private static final BigDecimal TAX_RATE = new BigDecimal(12);

	public WirelessEarphonesItem() {
		super(NAME, ITEM_PRICE, TAX_RATE);
	}
}
