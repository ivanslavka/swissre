package bigshop;

import java.util.HashSet;
import java.util.Set;

public class InputValidator {

	private final Set<String> filterKeys = new HashSet<>();

	public InputValidator() {
		this.filterKeys.add(PhoneCaseItem.NAME);
		this.filterKeys.add(PhoneInsuranceItem.NAME);
		this.filterKeys.add(SimCardItem.NAME);
		this.filterKeys.add(WiredEarphonesItem.NAME);
		this.filterKeys.add(WirelessEarphonesItem.NAME);
	}

	public boolean validateInput(String[] inputArray) {
		boolean validInput = true;
		for(String input : inputArray) {
			if(!this.filterKeys.contains(input)) {
				validInput = false;
				break;
			}
		}
		return validInput;
	}

	public String getValidInputKeys() {
		StringBuilder sb = new StringBuilder();
		for(String filterKey : this.filterKeys) {
			sb.append("\"");
			sb.append(filterKey);
			sb.append("\"");
			sb.append(" ");
		}
		return sb.toString();
	}
}
