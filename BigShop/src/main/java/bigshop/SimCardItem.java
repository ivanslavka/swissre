package bigshop;

import java.math.BigDecimal;
import java.util.LinkedList;

public class SimCardItem extends AbstractItem{

	public static final String NAME = "SIM card";

	private static final BigDecimal ITEM_PRICE = new BigDecimal(20);
	private static final BigDecimal TAX_RATE = new BigDecimal(12);
	public static final int MAX_NUMBER_OF_SIM_CARDS = 10;

	public SimCardItem() {
		super(NAME, ITEM_PRICE, TAX_RATE);
	}

	@Override
	public void addItem(LinkedList<CartItem> itemList) {
		if(itemList.size() == MAX_NUMBER_OF_SIM_CARDS){
			return;
		}
		itemList.add(new CartItem(this, false));
	}

	@Override
	public void addFreeItem(LinkedList<CartItem> itemList) {
		itemList.add(new CartItem(this, true));
	}
}
