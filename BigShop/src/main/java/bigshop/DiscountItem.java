package bigshop;

import java.math.BigDecimal;

public class DiscountItem extends AbstractItem {

	public static final String NAME = "DISCOUNT - ";

	public DiscountItem(AbstractItem item, BigDecimal discountRate){
		super(NAME + item.getItemName(), item.getItemPrice().multiply(discountRate).negate(), item.getItemTaxRate());
	}

}
