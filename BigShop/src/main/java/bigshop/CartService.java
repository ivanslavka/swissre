package bigshop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CartService {
	private final Map<String, LinkedList<CartItem>> cartContents = new LinkedHashMap<>();

	private static final int DEFAULT_QUANTITY = 1;
	private static final String DEFAULT_CURRENCY_SYMBOL = "CHF";

	private List<IDiscount> discountProcessorList = new ArrayList<>();

	public Map<String, LinkedList<CartItem>> getCartContents(){
		return this.cartContents;
	}

	public void addItemToCart(AbstractItem item) {
		if(!this.cartContents.containsKey(item.getItemName())) {
			this.cartContents.put(item.getItemName(), new LinkedList<>());
		}
		item.addItem(this.cartContents.get(item.getItemName()));
	}

	public void addFreeItemToCart(AbstractItem item) {
		if(!this.cartContents.containsKey(item.getItemName())) {
			this.cartContents.put(item.getItemName(), new LinkedList<>());
		}
		item.addFreeItem(this.cartContents.get(item.getItemName()));
	}

	public void removeItemFromCart(AbstractItem item) {
		if(this.cartContents.containsKey(item.getItemName())) {
			if(!this.cartContents.get(item.getItemName()).isEmpty()){
				item.removeItem(this.cartContents.get(item.getItemName()));
			}
			if(this.cartContents.get(item.getItemName()).isEmpty()) {
				this.cartContents.remove(item.getItemName());
			}
		}
	}

	public void applyDiscounts() {
		for(IDiscount discount : this.discountProcessorList){
			discount.applyDiscount(this);
		}
	}

	public BigDecimal calculateTotalPrice() {
		BigDecimal totalPrice = new BigDecimal(0);

		for(Entry<String, LinkedList<CartItem>> entry : this.cartContents.entrySet()) {
			for(CartItem item : entry.getValue()) {
				totalPrice = totalPrice.add(item.calculateItemPrice());
			}
		}
		return totalPrice.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}

	public BigDecimal calculateTotalTax() {
		BigDecimal totalTax = new BigDecimal(0);

		for(Entry<String, LinkedList<CartItem>> entry : this.cartContents.entrySet()) {
			for(CartItem item : entry.getValue()) {
				totalTax = totalTax.add(item.calculateItemTax());
			}
		}
		return totalTax.setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}

	public String buildReceipt(){
		StringBuilder sb = new StringBuilder();

		sb.append(String.format("%-30s %5s %10s\n", "Item", "Qty", "Price"))
		.append(String.format("%-30s %5s %10s\n", "----", "---", "-----"));

		for(Entry<String, LinkedList<CartItem>> entry : this.getCartContents().entrySet()) {
			for(CartItem item : entry.getValue()) {
				sb.append(String.format("%-30s %5s %10s %1s\n", item.getCatalogueItemName(), DEFAULT_QUANTITY, item.calculateItemPrice(), DEFAULT_CURRENCY_SYMBOL));
			}
		}

		sb.append(String.format("%-30s %16s\n", "----", "-----"))
		.append(String.format("%-30s %16.2f %1s\n", "Total", this.calculateTotalPrice(), "CHF"))
		.append(String.format("%-30s %16.2f %1s\n", "Total tax", this.calculateTotalTax(), "CHF"));

		return sb.toString();
	}

	public void addDiscountProcessor(IDiscount discountProcessor){
		this.discountProcessorList.add(discountProcessor);
	}
}
