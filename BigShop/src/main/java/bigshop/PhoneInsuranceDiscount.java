package bigshop;

import java.math.BigDecimal;

public class PhoneInsuranceDiscount implements IDiscount{

	private final BigDecimal DISCOUNT_RATE = new BigDecimal (0.2d);

	@Override
	public void applyDiscount(CartService cart){
		if(!cart.getCartContents().containsKey(PhoneInsuranceItem.NAME)){
			return;
		}

		int numberOfEarPhones = 0;
		if(cart.getCartContents().containsKey(WiredEarphonesItem.NAME)){
			numberOfEarPhones += cart.getCartContents().get(WiredEarphonesItem.NAME).size();
		}

		if(cart.getCartContents().containsKey(WirelessEarphonesItem.NAME)){
			numberOfEarPhones += cart.getCartContents().get(WirelessEarphonesItem.NAME).size();
		}

		int numberOfPhoneInsurances = cart.getCartContents().get(PhoneInsuranceItem.NAME).size();
		int numberOfPossibleDiscounts = Math.min(numberOfEarPhones, numberOfPhoneInsurances);
		for(int i = 0; i < numberOfPossibleDiscounts; i++){
			cart.addItemToCart(new DiscountItem(new PhoneInsuranceItem(), this.DISCOUNT_RATE));
		}
	}
}
