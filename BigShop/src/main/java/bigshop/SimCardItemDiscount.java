package bigshop;

public class SimCardItemDiscount implements IDiscount{

	private final int NUMBER_OF_MAX_SIM_CARDS = 10;
	private final double BUNDLE = 0.5;

	@Override
	public void applyDiscount(CartService cart){
		if(!cart.getCartContents().containsKey(SimCardItem.NAME)){
			return;
		}

		int numberOfSimCardsInCart = cart.getCartContents().get(SimCardItem.NAME).size();
		if(numberOfSimCardsInCart <= (this.NUMBER_OF_MAX_SIM_CARDS * this.BUNDLE)){
			for(int i = 0; i < numberOfSimCardsInCart; i++){
				cart.addFreeItemToCart(new SimCardItem());
			}
		} else {
			this.addMaxNumberOfSimCardsAndMakeEveryOtherFree(cart);
		}

	}

	private void addMaxNumberOfSimCardsAndMakeEveryOtherFree(CartService cart){
		int numberOfSimCardsInCart = cart.getCartContents().get(SimCardItem.NAME).size();
		for(int i = 0; i < (this.NUMBER_OF_MAX_SIM_CARDS - numberOfSimCardsInCart); i++){
			cart.addItemToCart(new SimCardItem());
		}
		for(int i = 0; i < (this.NUMBER_OF_MAX_SIM_CARDS * this.BUNDLE); i++){
			cart.getCartContents().get(SimCardItem.NAME).get(i).makeItemFree();
		}
	}
}
