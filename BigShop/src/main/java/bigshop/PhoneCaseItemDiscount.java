package bigshop;

import java.util.List;

public class PhoneCaseItemDiscount implements IDiscount{

	private final int BUNDLE = 4;

	@Override
	public void applyDiscount(CartService cart){
		if(!cart.getCartContents().containsKey(PhoneCaseItem.NAME)){
			return;
		}

		List<CartItem> phoneCaseList = cart.getCartContents().get(PhoneCaseItem.NAME);

		for(int i = 1; i <= phoneCaseList.size(); i++) {
			if(i % this.BUNDLE == 0) {
				phoneCaseList.get(i - 1).makeItemFree();
			}
		}
	}
}
