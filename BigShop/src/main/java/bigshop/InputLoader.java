package bigshop;

public class InputLoader {

	public void loadInputIntoCart(String[] inputArray, CartService cart) {
		for(String input : inputArray) {
			if(PhoneCaseItem.NAME.equals(input)) {
				cart.addItemToCart(new PhoneCaseItem());
			}

			if(PhoneInsuranceItem.NAME.equals(input)) {
				cart.addItemToCart(new PhoneInsuranceItem());
			}

			if(WirelessEarphonesItem.NAME.equals(input)) {
				cart.addItemToCart(new WirelessEarphonesItem());
			}

			if(WiredEarphonesItem.NAME.equals(input)) {
				cart.addItemToCart(new WiredEarphonesItem());
			}

			if(SimCardItem.NAME.equals(input)) {
				cart.addItemToCart(new SimCardItem());
			}
		}
	}
}
