package bigshop;

import java.math.BigDecimal;

public class WiredEarphonesItem extends AbstractItem{

	public static final String NAME = "wired earphone";

	private static final BigDecimal ITEM_PRICE = new BigDecimal(30);
	private static final BigDecimal TAX_RATE = new BigDecimal(12);

	public WiredEarphonesItem() {
		super(NAME, ITEM_PRICE, TAX_RATE);
	}
}
