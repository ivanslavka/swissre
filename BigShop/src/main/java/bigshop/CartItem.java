package bigshop;

import java.math.BigDecimal;

public class CartItem {
	private boolean isFree;
	private AbstractItem catalogueItem;

	public CartItem(AbstractItem catalogueItem, boolean isFree) {
		this.catalogueItem = catalogueItem;
		this.isFree = isFree;
	}

	public void makeItemFree(){
		this.isFree = true;
	}

	public boolean isItemFree(){
		return this.isFree;
	}

	public BigDecimal calculateItemPrice() {
		if(this.isFree) {
			return BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		}
		return this.catalogueItem.calculateItemPrice();
	}

	public BigDecimal calculateItemTax() {
		if(this.isFree) {
			return BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		}
		return this.catalogueItem.calculateItemTax();
	}

	public BigDecimal getCataloguePrice() {
		return this.catalogueItem.getItemPrice();
	}

	public String getCatalogueItemName() {
		return this.catalogueItem.getItemName();
	}
}
