package bigshop;

import java.util.HashMap;
import java.util.Map;

public class ItemCatalogue {
	public static final String ITEM_NOT_IN_CATALOGUE_ERROR = "Requested item is not in catalogue";

	private final Map<Integer, AbstractItem> items = new HashMap<>();

	public AbstractItem getItem(Integer itemCode) throws IllegalArgumentException{
		if(this.items.containsKey(itemCode)) {
			return this.items.get(itemCode);
		} else {
			throw new IllegalArgumentException(ITEM_NOT_IN_CATALOGUE_ERROR);
		}
	}

	public void addItem(Integer itemCode, AbstractItem item) {
		if(!this.items.containsKey(itemCode)) {
			this.items.put(itemCode, item);
		}
	}
}
