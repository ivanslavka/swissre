package bigshop;

import java.math.BigDecimal;
import java.util.LinkedList;

public class PhoneCaseItem extends AbstractItem {

	public static final String NAME = "phone case";

	private static final int BUNDLE = 4;
	private static final BigDecimal TAX_RATE = new BigDecimal(12);
	private static final BigDecimal ITEM_PRICE = new BigDecimal(10);

	public PhoneCaseItem() {
		super(NAME, ITEM_PRICE, TAX_RATE);
	}

	@Override
	public void addItem(LinkedList<CartItem> itemList) {
		if((itemList.size() + 1) % BUNDLE == 0) {
			itemList.add(new CartItem(this, true));
		} else {
			itemList.add(new CartItem(this, false));
		}
	}
}
