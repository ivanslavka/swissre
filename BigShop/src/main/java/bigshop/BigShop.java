package bigshop;

public class BigShop {

	public static void main(String[] args) {
		InputValidator inputValidator = new InputValidator();
		if(!inputValidator.validateInput(args)) {
			throw new IllegalArgumentException("Valid shopping items are: " + inputValidator.getValidInputKeys());
		}

		CartService cart = new CartService();
		cart.addDiscountProcessor(new SimCardItemDiscount());
		cart.addDiscountProcessor(new PhoneCaseItemDiscount());
		cart.addDiscountProcessor(new PhoneInsuranceDiscount());

		InputLoader inputLoader = new InputLoader();
		inputLoader.loadInputIntoCart(args, cart);
		cart.applyDiscounts();

		System.out.println(cart.buildReceipt());
	}
}
