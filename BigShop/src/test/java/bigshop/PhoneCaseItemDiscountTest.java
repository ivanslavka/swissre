package bigshop;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

public class PhoneCaseItemDiscountTest {

	@Test
	public void tenPhoneCasesInBasket_applyDiscount_everyFourthHasPriceZero(){
		int numberOfCases = 10;
		CartService cart = new CartService();
		for(int i = 0; i < numberOfCases; i++) {
			cart.addItemToCart(new PhoneCaseItem());
		}
		PhoneCaseItemDiscount discount = new PhoneCaseItemDiscount();

		// when
		discount.applyDiscount(cart);

		// then
		for(int i = 1; i <= numberOfCases; i++) {
			if(i % 4 == 0) {
				assertTrue(cart.getCartContents().get(PhoneCaseItem.NAME).get(i - 1).calculateItemPrice().compareTo(BigDecimal.ZERO) == 0);
			}
		}
	}
}
