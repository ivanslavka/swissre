package bigshop;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class CartTest {

	@Test
	public void itemIsAddedToItemGroup() {
		SimCardItem simCardItem = new SimCardItem();
		CartService cart = new CartService();

		// when
		cart.addItemToCart(simCardItem);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		// then
		assertTrue(!cartContents.isEmpty());
		assertTrue(!cartContents.get(simCardItem.getItemName()).isEmpty());
	}

	@Test
	public void twoItemsInItemGroup_oneItemIsRemovedFromItemGroup_oneItemInItemGroupRemains() {
		int initialItemGroupSize = 2;
		PhoneInsuranceItem phoneInsuranceItem = new PhoneInsuranceItem();
		CartService cart = new CartService();
		for(int i = 0; i < initialItemGroupSize; i++) {
			cart.addItemToCart(phoneInsuranceItem);
		}

		// when
		cart.removeItemFromCart(phoneInsuranceItem);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		// then
		assertTrue(!cartContents.isEmpty());
		assertTrue(!cartContents.get(phoneInsuranceItem.getItemName()).isEmpty());
		assertTrue(cartContents.get(phoneInsuranceItem.getItemName()).size() == 1);
	}

	@Test
	public void itemGroupCantExistWhenLastItemFromGroupRemoved() {
		SimCardItem simCardItem = new SimCardItem();
		CartService cart = new CartService();
		cart.addItemToCart(simCardItem);

		// when
		cart.removeItemFromCart(simCardItem);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		// then
		assertTrue(!cartContents.containsKey(simCardItem.getItemName()));
	}

	@Test
	public void insuranceDiscountAddedWhenWirelessEarphonesBought() {
		CartService cart = new CartService();
		cart.addDiscountProcessor(new PhoneInsuranceDiscount());
		cart.addItemToCart(new PhoneInsuranceItem());
		cart.addItemToCart(new WirelessEarphonesItem());

		// when
		cart.applyDiscounts();
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		// then
		assertTrue(!cartContents.isEmpty());
		assertTrue(cartContents.containsKey(DiscountItem.NAME + PhoneInsuranceItem.NAME));
	}

	@Test
	public void calculateTotalValueForOnePhoneCase() {
		CartService cart = new CartService();
		cart.addItemToCart(new PhoneCaseItem());
		CartItem phoneCase = new CartItem(new PhoneCaseItem(), false);

		// when
		BigDecimal totalPrice = cart.calculateTotalPrice();

		// then
		assertTrue(totalPrice.equals(phoneCase.calculateItemPrice()));
	}

	@Test
	public void calculateTotalValueForOnePhoneCaseAndTwoSimCards() {
		CartService cart = new CartService();
		cart.addItemToCart(new PhoneCaseItem());
		cart.addItemToCart(new SimCardItem());
		CartItem phoneCase = new CartItem(new PhoneCaseItem(), false);
		CartItem simCard = new CartItem(new SimCardItem(), false);
		CartItem simCardFree = new CartItem(new SimCardItem(), true);

		// when
		BigDecimal totalPrice = cart.calculateTotalPrice();

		// then
		assertTrue(totalPrice.equals(phoneCase.calculateItemPrice().add(simCard.calculateItemPrice()).add(simCardFree.calculateItemPrice())));
	}

	@Test
	public void calculateTotalValueForOnePhoneInsuranceOneWirelessEarphonesAndOnePhoneInsuranceDiscount() {
		CartService cart = new CartService();
		cart.addDiscountProcessor(new PhoneInsuranceDiscount());
		cart.addItemToCart(new WirelessEarphonesItem());
		cart.addItemToCart(new PhoneInsuranceItem());
		cart.applyDiscounts();

		// when
		BigDecimal totalPrice = cart.calculateTotalPrice();

		// then
		assertTrue(totalPrice.compareTo(new BigDecimal(152)) == 0);
	}

	@Test
	public void oneSimCardFourPhoneCasesOneWirelessEarphoneOnePhoneInsurance_applyDiscount_twoSimCardsTotalOneIsFreeFourPhoneCasesTotalOneIsFreeOnePhoneInsuranceDiscount() {
		int phoneCasesQty = 4;
		int freePhoneCasesExpected = 1;
		CartService cart = new CartService();
		cart.addDiscountProcessor(new PhoneInsuranceDiscount());
		cart.addDiscountProcessor(new SimCardItemDiscount());
		cart.addItemToCart(new SimCardItem());
		cart.addItemToCart(new WirelessEarphonesItem());
		cart.addItemToCart(new PhoneInsuranceItem());
		for(int i = 0; i < phoneCasesQty; i++){
			cart.addItemToCart(new PhoneCaseItem());
		}

		// when
		cart.applyDiscounts();

		// then
		assertTrue(cart.getCartContents().get(SimCardItem.NAME).size() == 2);
		assertTrue(cart.getCartContents().get(DiscountItem.NAME + PhoneInsuranceItem.NAME).size() == 1);
		int freePhoneCasesAfterDiscount = 0;
		for(CartItem item : cart.getCartContents().get(PhoneCaseItem.NAME)){
			if(item.isItemFree()){
				freePhoneCasesAfterDiscount++;
			}
		}

		Assert.assertEquals(freePhoneCasesExpected, freePhoneCasesAfterDiscount);
	}

	@Test
	public void emptyCart_buildReceipt_buildHeaderAndFooter() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%-30s %5s %10s\n", "Item", "Qty", "Price"))
		.append(String.format("%-30s %5s %10s\n", "----", "---", "-----"))
		.append(String.format("%-30s %16s\n", "----", "-----"))
		.append(String.format("%-30s %16.2f %1s\n", "Total", 0.00d, "CHF"))
		.append(String.format("%-30s %16.2f %1s\n", "Total tax", 0.00d, "CHF"));
		String expectedOutput = sb.toString();
		CartService cart = new CartService();
		//when
		String actualOutput = cart.buildReceipt();

		// then
		Assert.assertEquals(expectedOutput, actualOutput);
	}

	@Test
	public void cartContainsOnePhoneCase_buildReceiptForOnePhoneCase() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%-30s %5s %10s\n", "Item", "Qty", "Price"))
		.append(String.format("%-30s %5s %10s\n", "----", "---", "-----"))
		.append(String.format("%-30s %5d %10.2f %1s\n", PhoneCaseItem.NAME, 1, 11.2d, "CHF"))
		.append(String.format("%-30s %16s\n", "----", "-----"))
		.append(String.format("%-30s %16.2f %1s\n", "Total", 11.2d, "CHF"))
		.append(String.format("%-30s %16.2f %1s\n", "Total tax", 1.2d, "CHF"));
		String expectedOutput = sb.toString();
		CartService cart = new CartService();
		cart.addItemToCart(new PhoneCaseItem());

		//when
		String actualOutput = cart.buildReceipt();

		// then
		Assert.assertEquals(expectedOutput, actualOutput);
	}

	@Test
	public void cartContainsOnePhoneCaseTwoSimCards_buildReceipt(){
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%-30s %5s %10s\n", "Item", "Qty", "Price"))
		.append(String.format("%-30s %5s %10s\n", "----", "---", "-----"))
		.append(String.format("%-30s %5d %10.2f %1s\n", PhoneCaseItem.NAME, 1, 11.2d, "CHF"))
		.append(String.format("%-30s %5d %10.2f %1s\n", SimCardItem.NAME, 1, 22.4d, "CHF"))
		.append(String.format("%-30s %5d %10.2f %1s\n", SimCardItem.NAME, 1, 0d, "CHF"))
		.append(String.format("%-30s %16s\n", "----", "-----"))
		.append(String.format("%-30s %16.2f %1s\n", "Total", 33.6d, "CHF"))
		.append(String.format("%-30s %16.2f %1s\n", "Total tax", 3.6d, "CHF"));
		String expectedOutput = sb.toString();
		CartService cart = new CartService();
		cart.addDiscountProcessor(new SimCardItemDiscount());
		cart.addItemToCart(new PhoneCaseItem());
		cart.addItemToCart(new SimCardItem());
		cart.applyDiscounts();

		//when
		String actualOutput = cart.buildReceipt();

		// then
		Assert.assertEquals(expectedOutput, actualOutput);
	}

	@Test
	public void cartContainsOneWirelessEarphonesOnePhoneInsuranceAndOnePhoneInsuranceDiscount_buildReceipt() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%-30s %5s %10s\n", "Item", "Qty", "Price"))
		.append(String.format("%-30s %5s %10s\n", "----", "---", "-----"))
		.append(String.format("%-30s %5d %10.2f %1s\n", WirelessEarphonesItem.NAME, 1, 56d, "CHF"))
		.append(String.format("%-30s %5d %10.2f %1s\n", PhoneInsuranceItem.NAME, 1, 120d, "CHF"))
		.append(String.format("%-30s %5d %10.2f %1s\n", DiscountItem.NAME + PhoneInsuranceItem.NAME, 1, -24d, "CHF"))
		.append(String.format("%-30s %16s\n", "----", "-----"))
		.append(String.format("%-30s %16.2f %1s\n", "Total", 152d, "CHF"))
		.append(String.format("%-30s %16.2f %1s\n", "Total tax", 6d, "CHF"));
		String expectedOutput = sb.toString();
		CartService cart = new CartService();
		cart.addDiscountProcessor(new PhoneInsuranceDiscount());
		cart.addItemToCart(new WirelessEarphonesItem());
		cart.addItemToCart(new PhoneInsuranceItem());
		cart.applyDiscounts();

		//when
		String actualOutput = cart.buildReceipt();

		// then
		Assert.assertEquals(expectedOutput, actualOutput);
	}

	@Test
	public void cartContainsFivePhoneCases_buildReceipt() {
		int numberOfPhoneCases = 5;
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%-30s %5s %10s\n", "Item", "Qty", "Price"))
		.append(String.format("%-30s %5s %10s\n", "----", "---", "-----"))
		.append(String.format("%-30s %5d %10.2f %1s\n", PhoneCaseItem.NAME, 1, 11.2d, "CHF"))
		.append(String.format("%-30s %5d %10.2f %1s\n", PhoneCaseItem.NAME, 1, 11.2d, "CHF"))
		.append(String.format("%-30s %5d %10.2f %1s\n", PhoneCaseItem.NAME, 1, 11.2d, "CHF"))
		.append(String.format("%-30s %5d %10.2f %1s\n", PhoneCaseItem.NAME, 1, 0d, "CHF"))
		.append(String.format("%-30s %5d %10.2f %1s\n", PhoneCaseItem.NAME, 1, 11.2d, "CHF"))
		.append(String.format("%-30s %16s\n", "----", "-----"))
		.append(String.format("%-30s %16.2f %1s\n", "Total", 44.8d, "CHF"))
		.append(String.format("%-30s %16.2f %1s\n", "Total tax", 4.8d, "CHF"));
		String expectedOutput = sb.toString();
		CartService cart = new CartService();
		for(int i = 0; i < numberOfPhoneCases; i++) {
			cart.addItemToCart(new PhoneCaseItem());
		}

		//when
		String actualOutput = cart.buildReceipt();

		// then
		Assert.assertEquals(expectedOutput, actualOutput);
	}

	@Test
	public void oneSimCardOnePhoneCaseOnePhoneInsurance_calculateTotalTax_taxCalculated(){
		BigDecimal expectedTotalTax = new BigDecimal(3.6d).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		CartService cart = new CartService();
		cart.addItemToCart(new SimCardItem());
		cart.addItemToCart(new PhoneCaseItem());
		cart.addItemToCart(new PhoneInsuranceItem());

		BigDecimal totalTax = cart.calculateTotalTax();

		assertTrue(expectedTotalTax.compareTo(totalTax) == 0);
	}

	@Test
	public void oneSimCardOneFreeSimCardOneWiredEarphoneOnePhoneCaseOnePhoneInsuranceOneDiscount_calculateTotalTax_taxCalculated(){
		BigDecimal expectedTotalTax = new BigDecimal(7.2d).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		CartService cart = new CartService();
		cart.addItemToCart(new SimCardItem());
		cart.addItemToCart(new PhoneCaseItem());
		cart.addItemToCart(new PhoneInsuranceItem());
		cart.addItemToCart(new WiredEarphonesItem());
		cart.addFreeItemToCart(new SimCardItem());
		cart.applyDiscounts();

		BigDecimal totalTax = cart.calculateTotalTax();

		assertTrue(expectedTotalTax.compareTo(totalTax) == 0);
	}
}

