package bigshop;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class InputValidatorTest {

	private String[] inputArray;
	private boolean isInputValid;

	public InputValidatorTest(String[] inputArray, boolean isInputValid) {
		this.inputArray = inputArray;
		this.isInputValid = isInputValid;
	}

	// creates the test data
	@Parameters
	public static Collection<Object[]> data() {
		Object[][] data = new Object[][] {
			{ new String[] {""}, false },
			{ new String[] {null}, false },
			{ new String[] {"phone insurance", "phone ca"}, false } ,
			{ new String[] {"phone insurance", null}, false } ,
			{ new String[] {"phone insurance", ""}, false } ,
			{ new String[] {"phone insurance", " "}, false } ,
			{ new String[] {"phone insurance"}, true  },
			{ new String[] {"phone insurance", "phone case"}, true } ,
			{ new String[] {"SIM card", "SIM card"}, true } ,
			{ new String[] {"wired earphone"}, true } ,
			{ new String[] {"wireless earphone"}, true }
		};
		return Arrays.asList(data);
	}

	@Test
	public void testValidateInput() {

		InputValidator validator = new InputValidator();
		boolean isInputValid = validator.validateInput(this.inputArray);

		assertEquals(this.isInputValid, isInputValid);
	}
}
