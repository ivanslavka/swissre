package bigshop;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	CartTest.class,
	ItemCatalogueTest.class,
	CartItemTest.class,
	InputValidatorTest.class,
	InputLoaderTest.class,
	PhoneInsuranceDiscountTest.class,
	PhoneCaseItemDiscountTest.class,
	SimCardItemDiscountTest.class,
	SimCardItemTest.class
})

public class FeatureTestSuite {

}
