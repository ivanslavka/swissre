package bigshop;

import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.Map;

import org.junit.Test;

public class InputLoaderTest {

	@Test
	public void loadingOnePhoneCase_onePhoneCaseKeyInCart() {
		CartService cart = new CartService();
		InputLoader inputLoader = new InputLoader();
		String[] inputArray = {PhoneCaseItem.NAME};

		inputLoader.loadInputIntoCart(inputArray, cart);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		assertTrue(cartContents.size() == 1);
		assertTrue(cartContents.containsKey(PhoneCaseItem.NAME));
	}

	@Test
	public void loadingOnePhoneInsurance_onePhoneInsuranceKeyInCart() {
		CartService cart = new CartService();
		InputLoader inputLoader = new InputLoader();
		String[] inputArray = {PhoneInsuranceItem.NAME};

		inputLoader.loadInputIntoCart(inputArray, cart);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		assertTrue(cartContents.size() == 1);
		assertTrue(cartContents.containsKey(PhoneInsuranceItem.NAME));
	}

	@Test
	public void loadingOneWirelessEarphones_OneWirelessEarphonesKeyInCart() {
		CartService cart = new CartService();
		InputLoader inputLoader = new InputLoader();
		String[] inputArray = {WirelessEarphonesItem.NAME};

		inputLoader.loadInputIntoCart(inputArray, cart);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		assertTrue(cartContents.size() == 1);
		assertTrue(cartContents.containsKey(WirelessEarphonesItem.NAME));
	}

	@Test
	public void loadingOneWiredEarphones_oneWiredEarphonesKeyInCart() {
		CartService cart = new CartService();
		InputLoader inputLoader = new InputLoader();
		String[] inputArray = {WiredEarphonesItem.NAME};

		inputLoader.loadInputIntoCart(inputArray, cart);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		assertTrue(cartContents.size() == 1);
		assertTrue(cartContents.containsKey(WiredEarphonesItem.NAME));
	}

	@Test
	public void loadingTwoWiredEarphones_oneWiredEarphonesKeyInCart() {
		CartService cart = new CartService();
		InputLoader inputLoader = new InputLoader();
		String[] inputArray = {WiredEarphonesItem.NAME, WiredEarphonesItem.NAME};

		inputLoader.loadInputIntoCart(inputArray, cart);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		assertTrue(cartContents.size() == 1);
		assertTrue(cartContents.containsKey(WiredEarphonesItem.NAME));
	}

	@Test
	public void loadingOneSimCard_oneSimCardKeyInCart() {
		CartService cart = new CartService();
		InputLoader inputLoader = new InputLoader();
		String[] inputArray = {SimCardItem.NAME};

		inputLoader.loadInputIntoCart(inputArray, cart);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		assertTrue(cartContents.size() == 1);
		assertTrue(cartContents.containsKey(SimCardItem.NAME));
	}

	@Test
	public void loadingOneSimCardAndOnePhoneCase_oneSimCardAndOnePhoneCaseKeyInCart() {
		CartService cart = new CartService();
		InputLoader inputLoader = new InputLoader();
		String[] inputArray = {SimCardItem.NAME, PhoneCaseItem.NAME};

		inputLoader.loadInputIntoCart(inputArray, cart);
		Map<String, LinkedList<CartItem>> cartContents = cart.getCartContents();

		assertTrue(cartContents.size() == 2);
		assertTrue(cartContents.containsKey(PhoneCaseItem.NAME));
		assertTrue(cartContents.containsKey(SimCardItem.NAME));
	}
}
