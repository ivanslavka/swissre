package bigshop;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PhoneInsuranceDiscountTest {

	@Test
	public void wirelessEarphonesAndInsuranceInBasket_applyDiscount_addedDiscountShouldHaveNegativeValue() {
		PhoneInsuranceItem phoneInsuranceItem = new PhoneInsuranceItem();
		CartService cart = new CartService();
		WirelessEarphonesItem wiredEarphone = new WirelessEarphonesItem();
		cart.addItemToCart(wiredEarphone);
		cart.addItemToCart(phoneInsuranceItem);

		PhoneInsuranceDiscount discount = new PhoneInsuranceDiscount();
		discount.applyDiscount(cart);

		assertTrue(!cart.getCartContents().isEmpty());
		assertTrue(cart.getCartContents().get(DiscountItem.NAME + PhoneInsuranceItem.NAME).getFirst().getCataloguePrice().signum() == -1);
	}

	@Test
	public void wiredEarphonesAndInsuranceInBasket_applyDiscount_addedDiscountShouldHaveNegativeValue() {
		PhoneInsuranceItem phoneInsuranceItem = new PhoneInsuranceItem();
		CartService cart = new CartService();
		WiredEarphonesItem wiredEarphone = new WiredEarphonesItem();
		cart.addItemToCart(wiredEarphone);
		cart.addItemToCart(phoneInsuranceItem);

		PhoneInsuranceDiscount discount = new PhoneInsuranceDiscount();
		discount.applyDiscount(cart);

		assertTrue(!cart.getCartContents().isEmpty());
		assertTrue(cart.getCartContents().get(DiscountItem.NAME + PhoneInsuranceItem.NAME).getFirst().getCataloguePrice().signum() == -1);
	}

	@Test
	public void simCardAndInsuranceInBasket_applyDiscount_thereShouldBeNoDiscountInBasket() {
		PhoneInsuranceItem phoneInsuranceItem = new PhoneInsuranceItem();
		CartService cart = new CartService();
		SimCardItem simCard = new SimCardItem();
		cart.addItemToCart(simCard);
		cart.addItemToCart(phoneInsuranceItem);

		PhoneInsuranceDiscount discount = new PhoneInsuranceDiscount();
		discount.applyDiscount(cart);

		assertTrue(!cart.getCartContents().isEmpty());
		assertTrue(!cart.getCartContents().containsKey(DiscountItem.NAME + PhoneInsuranceItem.NAME));
	}

	@Test
	public void twoWirelessEarphonesAndTwoInsuranceInBasket_applyDiscount_addedTwoDiscounts() {
		PhoneInsuranceItem phoneInsuranceItem = new PhoneInsuranceItem();
		CartService cart = new CartService();
		WirelessEarphonesItem wiredEarphone = new WirelessEarphonesItem();
		cart.addItemToCart(wiredEarphone);
		cart.addItemToCart(wiredEarphone);
		cart.addItemToCart(phoneInsuranceItem);
		cart.addItemToCart(phoneInsuranceItem);

		PhoneInsuranceDiscount discount = new PhoneInsuranceDiscount();
		discount.applyDiscount(cart);

		assertTrue(cart.getCartContents().get(DiscountItem.NAME + PhoneInsuranceItem.NAME).size() == 2);
	}

	@Test
	public void oneWirelessEarphonesAndTwoInsuranceInBasket_applyDiscount_addedOneDiscount() {
		PhoneInsuranceItem phoneInsuranceItem = new PhoneInsuranceItem();
		CartService cart = new CartService();
		WirelessEarphonesItem wiredEarphone = new WirelessEarphonesItem();
		cart.addItemToCart(wiredEarphone);
		cart.addItemToCart(phoneInsuranceItem);

		PhoneInsuranceDiscount discount = new PhoneInsuranceDiscount();
		discount.applyDiscount(cart);

		assertTrue(cart.getCartContents().get(DiscountItem.NAME + PhoneInsuranceItem.NAME).size() == 1);
	}

	@Test
	public void TwoWirelessEarphonesAndOneInsuranceInBasket_applyDiscount_addedOneDiscount() {
		PhoneInsuranceItem phoneInsuranceItem = new PhoneInsuranceItem();
		CartService cart = new CartService();
		WirelessEarphonesItem wiredEarphone = new WirelessEarphonesItem();
		cart.addItemToCart(wiredEarphone);
		cart.addItemToCart(phoneInsuranceItem);

		PhoneInsuranceDiscount discount = new PhoneInsuranceDiscount();
		discount.applyDiscount(cart);

		assertTrue(cart.getCartContents().get(DiscountItem.NAME + PhoneInsuranceItem.NAME).size() == 1);
	}

	@Test
	public void oneWirelessEarphonesAndNoInsuranceInBasket_applyDiscount_noDiscount() {
		CartService cart = new CartService();
		WirelessEarphonesItem wiredEarphone = new WirelessEarphonesItem();
		cart.addItemToCart(wiredEarphone);

		PhoneInsuranceDiscount discount = new PhoneInsuranceDiscount();
		discount.applyDiscount(cart);

		assertTrue(!cart.getCartContents().containsKey(DiscountItem.NAME + PhoneInsuranceItem.NAME));
	}
}
