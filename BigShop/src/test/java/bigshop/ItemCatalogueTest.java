package bigshop;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ItemCatalogueTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	@Test
	public void retrievingNonExistingItemInCatalogueThrowsException() {
		Integer nonExistingItemCode = -1;

		this.expectedException.expect(IllegalArgumentException.class);
		this.expectedException.expectMessage(ItemCatalogue.ITEM_NOT_IN_CATALOGUE_ERROR);
		ItemCatalogue catalogue = new ItemCatalogue();

		catalogue.getItem(nonExistingItemCode);
	}
}
