package bigshop;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SimCardItemDiscountTest {

	@Test
	public void threeSimCardsInBasket_applyDiscount_sixSimCardsInBasketThreeWithPriceZero(){
		int expectedNumberOfFreeSim = 3;
		CartService cart = new CartService();
		for(int i = 0; i < 3; i++) {
			cart.addItemToCart(new SimCardItem());
		}

		// when
		SimCardItemDiscount discount = new SimCardItemDiscount();
		discount.applyDiscount(cart);

		// then
		int numberOfFreeItems = 0;
		for(CartItem item : cart.getCartContents().get(SimCardItem.NAME)){
			if(item.isItemFree()){
				numberOfFreeItems++;
			}
		}

		assertEquals(expectedNumberOfFreeSim, numberOfFreeItems);
		assertTrue(cart.getCartContents().get(SimCardItem.NAME).size() == 6);
	}

	@Test
	public void tenSimCardsInBasket_applyDiscount_tenSimCardsInBasketFiveSimCardsHavePriceZero(){
		int expectedNumberOfFreeSim = 5;
		CartService cart = new CartService();
		for(int i = 0; i < 10; i++) {
			cart.addItemToCart(new SimCardItem());
		}

		// when
		SimCardItemDiscount discount = new SimCardItemDiscount();
		discount.applyDiscount(cart);

		// then
		int numberOfFreeItems = 0;
		for(CartItem item : cart.getCartContents().get(SimCardItem.NAME)){
			if(item.isItemFree()){
				numberOfFreeItems++;
			}
		}

		assertEquals(expectedNumberOfFreeSim, numberOfFreeItems);
		assertTrue(cart.getCartContents().get(SimCardItem.NAME).size() == 10);
	}
}
