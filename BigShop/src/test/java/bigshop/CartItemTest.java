package bigshop;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

public class CartItemTest {

	@Test
	public void itemIsFree_CartItemPriceIsZero() {
		CartItem cartItem = new CartItem(new SimCardItem(), true);

		// when
		BigDecimal itemPrice = cartItem.calculateItemPrice();

		assertTrue(itemPrice.compareTo(BigDecimal.ZERO) == 0);
	}

	@Test
	public void itemIsInsurance_CartItemPriceHasCataloguePrice() {

		CartItem cartItem = new CartItem(new PhoneInsuranceItem(), false);
		BigDecimal itemPrice = cartItem.calculateItemPrice();

		assertTrue(itemPrice.compareTo(cartItem.getCataloguePrice()) == 0);
	}

	@Test
	public void cartItemPriceHasTaxCalculated() {
		CartItem cartItem = new CartItem(new SimCardItem(), false);
		BigDecimal taxRate = new BigDecimal(1.12d).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		// when
		BigDecimal itemPrice = cartItem.calculateItemPrice();

		assertTrue(itemPrice.compareTo(cartItem.getCataloguePrice().multiply(taxRate)) == 0);
	}
}
