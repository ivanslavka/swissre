package bigshop;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.LinkedList;

import org.junit.Test;

public class SimCardItemTest {

	@Test
	public void inserting11thSimCard_max10SimCardsInItemList() {
		LinkedList<CartItem> simCardList = new LinkedList<>();
		SimCardItem simCardItem = new SimCardItem();
		for(int i = 0; i < SimCardItem.MAX_NUMBER_OF_SIM_CARDS; i++){
			simCardItem.addItem(simCardList);
		}
		// when
		simCardItem.addItem(simCardList);

		// then
		assertTrue(simCardList.size() == SimCardItem.MAX_NUMBER_OF_SIM_CARDS);
	}

	@Test
	public void testCalculateSimCardItemTax12Percent() {
		SimCardItem simCardItem = new SimCardItem();
		BigDecimal expectedItemTax = new BigDecimal(2.4d).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		// when
		BigDecimal itemTax = simCardItem.calculateItemTax();

		// then
		assertTrue(expectedItemTax.compareTo(itemTax) == 0);
	}

	@Test
	public void testCalculateSimCardItemPriceWithTax12Percent() {
		SimCardItem simCardItem = new SimCardItem();
		BigDecimal expectedItemTax = new BigDecimal(22.4d).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		// when
		BigDecimal itemTax = simCardItem.calculateItemPrice();

		// then
		assertTrue(expectedItemTax.compareTo(itemTax) == 0);
	}
}
